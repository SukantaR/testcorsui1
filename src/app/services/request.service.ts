import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  constructor(private http: HttpClient) { }

  callingCORSApi() {
    let request: any = {
      "date":"2021-07-09T00:20:20.4078495+05:30",
      "temperatureC":49,
      "temperatureF":120,
      "summary":"Chilly"
   };
    let headers: any;
    headers = new Headers();
    return this.http.post('http://localhost/TestCORS/api/weather', request, { headers: headers });
  }
}
